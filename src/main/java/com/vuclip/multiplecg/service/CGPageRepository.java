/**
 *
 */
package com.vuclip.multiplecg.service;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.vuclip.multiplecg.model.CGPage;

/**
 * @author avinash
 *
 */
public interface CGPageRepository extends MongoRepository<CGPage, String> {

	CGPage findByContextName(String contextName);
}
