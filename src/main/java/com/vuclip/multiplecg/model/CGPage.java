/**
 *
 */
package com.vuclip.multiplecg.model;

import java.util.List;

import javax.persistence.Id;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author avinash
 *
 */
@Document
public class CGPage {
	@Id
	private String id;
	private String contextName;
	private String cgImageUrl;
	private String cgText;
	private String smsShortCode;
	private String smsBody;
	@Transient
	private MultipartFile cgImageFile;
	@Transient
	private String smsHeader;
	@Transient
	private List<String> cgTexts;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContextName() {
		return contextName;
	}

	public void setContextName(String contextName) {
		this.contextName = contextName;
	}

	public String getCgImageUrl() {
		return cgImageUrl;
	}

	public void setCgImageUrl(String cgImageUrl) {
		this.cgImageUrl = cgImageUrl;
	}

	public String getCgText() {
		return cgText;
	}

	public void setCgText(String cgText) {
		this.cgText = cgText;
	}

	public String getSmsShortCode() {
		return smsShortCode;
	}

	public void setSmsShortCode(String smsShortCode) {
		this.smsShortCode = smsShortCode;
	}

	public String getSmsBody() {
		return smsBody;
	}

	public void setSmsBody(String smsBody) {
		this.smsBody = smsBody;
	}

	public MultipartFile getCgImageFile() {
		return cgImageFile;
	}

	public void setCgImageFile(MultipartFile cgImageFile) {
		this.cgImageFile = cgImageFile;
	}

	public String getSmsHeader() {
		return smsHeader;
	}

	public void setSmsHeader(String smsHeader) {
		this.smsHeader = smsHeader;
	}

	public List<String> getCgTexts() {
		return cgTexts;
	}

	public void setCgTexts(List<String> cgTexts) {
		this.cgTexts = cgTexts;
	}

	@Override
	public String toString() {
		return "CGPage [id=" + id + ", contextName=" + contextName + ", cgImageUrl=" + cgImageUrl + ", cgText=" + cgText
				+ ", smsShortCode=" + smsShortCode + ", smsBody=" + smsBody + "]";
	}

}
