package com.vuclip.multiplecg.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "com.vuclip")
@EnableMongoRepositories(basePackages = "com.vuclip")
public class MultipleCgProjectInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(MultipleCgProjectInitializer.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(MultipleCgProjectInitializer.class, args);
	}
}
