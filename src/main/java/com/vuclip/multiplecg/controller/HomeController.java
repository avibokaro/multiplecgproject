/**
 *
 */
package com.vuclip.multiplecg.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author avinash
 *
 */
@Controller
public class HomeController {

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String renderCgPage() {
		return "views/home";
	}
}
