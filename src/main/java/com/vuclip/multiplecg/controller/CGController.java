/**
 *
 */
package com.vuclip.multiplecg.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.vuclip.multiplecg.model.CGPage;
import com.vuclip.multiplecg.service.CGPageRepository;
import com.vuclip.multiplecg.service.StorageService;

/**
 * @author avinash
 *
 */
@Controller
public class CGController {

	@Autowired
	private CGPageRepository cgPageRepository;

	@Autowired
	private StorageService storageService;

	List<String> files = new ArrayList<String>();

	@RequestMapping(value = "/{path}", method = RequestMethod.GET)
	public String showCGPage(@PathVariable String path, Model model) {
		CGPage cgPage=cgPageRepository.findByContextName(path);
		if (cgPage != null) {
			cgPage.setSmsHeader(prepareSmsHeader(cgPage));
			cgPage.setCgTexts(prepareSmsTexts(cgPage));
		}
		model.addAttribute("cgPage", cgPage);
		return "cgPage";
	}

	private List<String> prepareSmsTexts(CGPage cgPage) {
		List<String> cgTexts = Collections.emptyList();
		if (org.springframework.util.StringUtils.hasText(cgPage.getCgText())) {
			cgTexts = new ArrayList<>();
			if (cgPage.getCgText().contains(";")) {
				cgTexts.addAll(Arrays.asList(cgPage.getCgText().split(";")));
			} else {
				cgTexts.add(cgPage.getCgText());
			}
		}
		return cgTexts;
	}

	private String prepareSmsHeader(CGPage cgPage) {
		return "sms:" + cgPage.getSmsShortCode() + "?body=" + cgPage.getSmsBody();
	}

	@RequestMapping(value = "/addCGPage", method = RequestMethod.GET)
	public String addCgPage_get(Model model) {
		model.addAttribute("cgPage", new CGPage());
		return "views/addCGPage";
	}

	@RequestMapping(value = "/addCGPage", method = RequestMethod.POST)
	public String addCgPage_post(CGPage cgPage, Model model, HttpServletRequest request) {
		if (cgPage.getCgImageFile() != null) {
			cgPage.setCgImageUrl(getCgImageLink(cgPage, request));
		}
		cgPageRepository.save(cgPage);
		storageService.store(cgPage.getCgImageFile());
		return "redirect:showAllCGPages";
	}

	private String getCgImageLink(CGPage cgPage, HttpServletRequest request) {
		/*
		 * // String cgImageLink = request.getScheme() + "://" + //
		 * request.getServerName(); if (request.getServerPort() > 0) {
		 * cgImageLink += ":" + request.getServerPort(); }
		 */
		String cgImageLink = "imageRepo/" + cgPage.getCgImageFile().getOriginalFilename();
		return cgImageLink;
	}

	@RequestMapping(value = "/editCGPage/{id}", method = RequestMethod.GET)
	public String editCgPage_get(@PathVariable String id, Model model) {
		model.addAttribute("cgPage", cgPageRepository.findOne(id));
		return "views/editCGPage";
	}

	@RequestMapping(value = "/editCGPage", method = RequestMethod.POST)
	public String editCgPage_post(CGPage cgPage, HttpServletRequest request) {
		if (cgPage.getCgImageFile() != null) {
			cgPage.setCgImageUrl(getCgImageLink(cgPage, request));
		}
		cgPageRepository.save(cgPage);
		storageService.store(cgPage.getCgImageFile());
		return "redirect:showAllCGPages";
	}

	@RequestMapping(value = "/deleteCGPage/{id}", method = RequestMethod.GET)
	public String deleteCgPage(@PathVariable String id, Model model) {
		cgPageRepository.delete(id);
		return "redirect:../showAllCGPages";
	}

	@RequestMapping(value = "/showAllCGPages", method = RequestMethod.GET)
	public String showAllCgPages(CGPage cgPage, Model model, HttpServletRequest request) {
		List<CGPage> cgPages = cgPageRepository.findAll();
		model.addAttribute("cgPages", cgPages);
		return "views/showAllCGPages";
	}

	@GetMapping("/gellallfiles")
	public String getListFiles(Model model) {
		model.addAttribute("files",
				files.stream()
				.map(fileName -> MvcUriComponentsBuilder
						.fromMethodName(CGController.class, "getFile", fileName).build().toString())
				.collect(Collectors.toList()));
		model.addAttribute("totalFiles", "TotalFiles: " + files.size());
		return "listFiles";
	}

	@GetMapping("/imageRepo/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> getFile(@PathVariable String filename) {
		Resource file = storageService.loadFile(filename);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}
}
